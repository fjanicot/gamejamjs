// Array of movies
const movies = [
    {
        id: 1,
        title: 'Twilight (saga)',
        summary: 'Une émo grincheuse pendant plusieurs années se demande si elle doit coucher avec un cadavre ou un chien',
    },
    {
        id: 2,
        title: 'Batman',
        summary: 'Un milliardaire dépense son argent et son temps dans le cosplay',
    },
    {
        id: 3,
        title: 'Batman V Superman',
        summary: 'Un millionnaire est effrayé par un extraterrestre',
    },
    {
        id: 4,
        title: 'La belle et la Bête',
        summary: 'Une relation zoophile et un syndrome de Stockholm finissent en mariage',
    },
    {
        id: 5,
        title: 'Ça',
        summary: 'Des gamins tabassent un SDF affamé déguisé en clown',
    },
    {
        id: 6,
        title: 'Captain America',
        summary: 'Un gars sous stéroïde gagne la guerre grâce à un frisbee',
    },
    {
        id: 7,
        title: 'Fast and Furious 8',
        summary: 'Un chauve fait équipe avec un chauve contre un autre chauve',
    },
    {
        id: 8,
        title: 'Hunger Games',
        summary: 'Une grande soeur réduit les chances de sa petite soeur d\'apparaître à la télévision',
    },
    {
        id: 9,
        title: 'Inception',
        summary: 'Un groupe de personnes fait une sieste',
    },
    {
        id: 10,
        title: 'Avengers : Infinity War',
        summary: 'Un géant rose ramasse des cailloux',
    },
    {
        id: 11,
        title: 'Le Roi Lion',
        summary: 'Après avoir perdu son père, un jeune garçon rejoint un groupe de hippies',
    },
    {
        id: 12,
        title: 'Maman j\'ai raté l\'avion',
        summary: 'Un enfant perturbé et en overdose de mac\'n\'cheese essaye de tuer des SDF',
    },
    {
        id: 13,
        title: 'Seul sur Mars',
        summary: 'Un mec fait pousser des patates',
    },
    {
        id: 14,
        title: 'Shrek',
        summary: 'Un mec apprend à aimer une fille sans filtres Instagram',
    },
    {
        id: 15,
        title: 'Spider-Man',
        summary: 'Un mec avait oublié son aspi-venin pour aller au musée',
    },
    {
        id: 16,
        title: 'The Dark Knight',
        summary: 'Un mec maquillé détruit une ville juste pour emmerder un milliardaire',
    },
    {
        id: 17,
        title: 'Star Wars : Les derniers Jedi',
        summary: 'Un vieux dépressif sort de sa retraite pour faire des blagues à son neveu',
    },
    {   id: 18,
        title: 'Her',
        summary: 'C\'est l\'histoire d\'un mec qui date Siri (mais en vrai c\'est Scarlett Johansson)',
    },
    {
        id: 19,
        title: 'The Revenant',
        summary: 'Histoire d\'amour entre un homme et un ours.',
    },
    {
        id: 20,
        title: 'Thor : Ragnarok',
        summary: 'Un mec préfère voir sa planète détruite plutôt que de vivre avec sa soeur.',
    },
    {
        id: 21,
        title: 'Star Wars : L\'empire contre-attaque',
        summary: 'Une grenouille qui parle convainc un jeune homme de tuer son père',
    },
    {
        id: 22,
        title: 'Thor',
        summary: 'Un enfant adopté veut tuer son grand frère parce qu\'il ne veut pas prêter son marteau',
    },
    {
        id: 23,
        title: 'Titanic',
        summary: 'Le Ice Bucket Challenge XXL tourne mal',
    },
    {
        id: 24,
        title: 'Megamind',
        summary: 'C\'est genre Jimmy neutron, mais bleu et chauve',
    },
    {
        id: 25,
        title: 'Indiana Jones',
        summary: 'Un professeur sèche le travail pour trouver une boîte placée au mauvais endroit.',
    },
    {
        id: 26,
        title: 'Un jour sans fin',
        summary: 'Un homme éteint son réveil matin 3176 fois.',
    },
    {
        id: 27,
        title: 'Sacré Graal',
        summary: 'Un groupe de chevaliers cherche une coupe, et aucun d\'eux n\'a de cheval.',
    },
    {
        id: 28,
        title: 'Charlie et la Chocolaterie',
        summary: 'Une fabrique de bonbons gagne des millions en incitant les gens à acheter ses produits pour avoir l\'occasion de visiter son usine qui enfreint de nombreuses lois du travail et de sécurité.',
    },
    {
        id: 29,
        title: 'Shining',
        summary: 'Le séjour Wonderbox hôtel de rêve tourne en stage de bûcheron',
    },
    {
        id: 30,
        title: 'Là-Haut',
        summary: 'Un vieil homme accroche quelques ballons à sa maison pour rendre hommage à son épouse décédée.',
    },
    {
        id: 31,
        title: 'Jurassic Park',
        summary: 'Un mec riche achète une île et fout des vieux lézards dedans.',
    },
    {
        id: 32,
        title: 'Matrix',
        summary: 'Un mec prend une pilule, du coup il apprend des trucs, mais il sait pas trop s\'il aime apprendre des trucs.',
    },
    {
        id: 33,
        title: 'Le Seigneur des Anneaux (Trilogie)',
        summary: 'Neuf heure pour aller au SAV du manège à bijoux, c\'est chaud d\'habiter à la campagne',
    },
    {
        id: 34,
        title: 'E.T',
        summary: 'Un immigré clandestin est poursuivi par les forces de l\'ordre',
    },
    {
        id: 35,
        title: 'Phone Game',
        summary: 'Il aurait mieux fait d\'acheter un iPhone',
    },
    {
        id: 36,
        title: 'La Chute',
        summary: 'Un mec grincheux passe son temps à râler dans un bunker',
    },
    {
        id: 37,
        title: 'Stargate, la porte des étoiles',
        summary: 'La porte s\'ouvrait pas, il est allé au CDI et ça s\'est ouvert.',
    },
    {
        id: 38,
        title: 'Sauvez Willy',
        summary: 'Un gamin vole un gros poisson dans un aquarium',
    },
    {
        id: 39,
        title: 'Die Hard : Une journée en enfer',
        summary: 'Un mec veut jouer à Jacques a dit mais l\'autre ne veut pas',
    },
    {
        id: 40,
        title: 'Die Hard : Piège de cristal',
        summary: 'Un groupe de communistes redistribue les richesses des méchants japonais',
    },
    {
        id: 41,
        title: 'Robin des bois',
        summary: 'Un groupe de communistes redistribue les richesses des méchants anglais',
    },
    {
        id: 42,
        title: 'Shutter Island',
        summary: 'Un flic mais en fait on c\'est pas si c\'est un flic (peut-être que non) est bloqué sur une île en pleine tempête',
    },
    {
        id: 43,
        title: 'Mission Impossible',
        summary: 'Un mec doit faire une mission pas facile',
    },
    {
        id: 44,
        title: 'The Social Network',
        summary: 'On voit la vie d\'un mec alors que normalement c\'est lui qui connait notre vie',
    },
    {
        id: 45,
        title: 'Armageddon',
        summary: 'Douze mecs partent chercher du pétrole dans l\'espace (mais pas pour préserver la planète hein, pour aller au-delà de la mondialisation)',
    },
    {
        id: 46,
        title: 'Ratatouille',
        summary: 'Un restaurant 4 étoiles gagne une 5e étoile parce qu\'un critique a aimé la bouffe de leur rat.',
    },
    {
        id: 47,
        title: 'The Rock',
        summary: 'Une manifestation musclé pour les retraites tourne mal à San Francisco',
    },
    {
        id: 48,
        title: 'Arthur et les Minimoys',
        summary: 'Chérie j\'ai rétréci les gosses version heroic fantasy',
    },
    {
        id: 49,
        title: 'Qu\'est-ce qu\'on a fait au Bon Dieu ?',
        summary: 'Le bestof des blagues racistes de la page 1 de Google',
    },
    {
        id: 50,
        title: 'Forrest Gump',
        summary: 'C\'est l\'histoire d\'un mec sur un banc qui raconte sa life en bouffant des chocolats.',
    },
    {
        id: 51,
        title: 'La Liste de Schindler',
        summary: 'C\'est un nazi il se rend compte que les nazis ils sont méchants, alors il va se reconvertir en plombier parce que les plombiers ils sont gentils.',
    },
    {
        id: 52,
        title: 'Tous en Scène',
        summary: 'C\'est la Star Academy mais Jean-Pascal c\'est vraiment une bête a poils',
    },
    {
        id: 53,
        title: 'Snowpiercer',
        summary: '1984 mais dans un train',
    },
    {
        id: 54,
        title: 'Kingsman : Services Secrets',
        summary: 'Le super-espion décide de créer son mini-lui avant que le super méchant qui zozote ne lui pique l\'idée.',
    },
    {
        id: 55,
        title: 'Into the wild',
        summary: 'C\'est l\'histoire d\'un mec qui aurait dû lire des livres de cuisine vegan au lieu de recueil de poésie.',
    },
    {
        id: 56,
        title: 'Porco Rosso',
        summary: 'C\'est l\'histoire d\'un cochon qui pilote un avion, et visiblement ça ne choque personne.',
    },
    {
        id: 57,
        title: 'Memento',
        summary: 'C\'est l\'histoire d\'un mec qui prend des photos parce qu\'il n\'a plus de place pour se faire tatouer.',
    },
    {
        id: 58,
        title: 'Avatar',
        summary: 'Un gars joue aux Schtroumpfs en réalité virtuelle.',
    },
    {
        id: 59,
        title: 'Mulan',
        summary: 'Une fille doit faire semblant d\'être un homme pour être prise au sérieux.',
    },
    {
        id: 60,
        title: 'Toy Story',
        summary: 'Une bande de mecs chelous se prennent pour des jouets et stalkent un enfant',
    },
    {
        id: 61,
        title: 'Luca',
        summary: 'Un poisson fait du vélo et mange des pâtes.',
    },
    {
        id: 62,
        title: 'Prison Break',
        summary: 'Manquerait plus que son prochain tatouage soit le plan du métro parisien',
    },
    {
        id: 63,
        title: 'Game of Thrones',
        summary: 'On attend toujours l\'hiver...',
    },
    {
        id: 64,
        title: 'La Reine des neiges',
        summary: 'On attend toujours la fin de l\'hiver...',
    },
    {
        id: 65,
        title: 'Suits',
        summary: 'Le mec a raté ses études sup mais a réussi à avoir Meghan Markle',
    },
    {
        id: 66,
        title: 'Stranger Things',
        summary: 'Des animaux mutants et des enfants qui disparaissent... Y a pas à dire, c\'était vraiment mieux avant !',
    },
    {
        id: 67,
        title: 'The 100',
        summary: 'C\'est comme Wall-E mais avec des adolescents',
    },
    {
        id: 68,
        title: 'Gossip Girl',
        summary: 'Un ado geignard se plaint d’être pauvre alors qu’il habite dans un loft à Brooklyn.',
    },
    {
        id: 69,
        title: 'Lost',
        summary: 'J\'ai pas compris la fin, elle porte bien son nom',
    },
    {
        id: 70,
        title: 'Doctor Who',
        summary: 'Le mec se fait tuer mais il revient à la vie avec un lifting en direct',
    },
    {
        id: 71,
        title: 'Mr. Robot',
        summary: 'Fight Club avec des hackers',
    },
]

// Containers
const startContainer = document.querySelector('.start-container');
const challengeContainer = document.querySelector('.challenge-container')
const endContainer = document.querySelector('.end-container');
const containersBestScores = document.querySelectorAll('.best-result');
const summaryParagraph = document.getElementById('summary');
let timerContainer = document.getElementById('timer');
const failMessage = document.getElementById('failMessage');
let goodAnswer;

// Buttons
const startButton = document.getElementById('start');
const answerButtons = document.querySelectorAll('div.responses button');
const restartButton = document.getElementById('replay');

// Scores
let score = 0;
let bestScore = localStorage.getItem('bestScore') ?? 0;
let displayScore = document.querySelector('.current-score');
let displayBestScore = document.querySelectorAll('.best-score');
const displayFinalScore = document.querySelector('.finalScore');



// Launch game
const start = () => {
    startContainer.remove();
    challengeContainer.classList.remove('hide');
    for(let answerButton of answerButtons){
        answerButton.addEventListener('click', (e) => {

            let isSuccess;

            if(e.target.innerText === goodAnswer['title'] ) {
                e.target.classList.add('success');
                score++;
                isSuccess = 1;
            }else{
                e.target.classList.add('error');
                isSuccess = 0;
            }

            for(answerButton of answerButtons) {
                answerButton.setAttribute('disabled', '');
            }
            clearInterval(timer);

            // Timer before changing container
            setTimeout(() => {
                if(isSuccess === 1){
                    nextMovie();
                }else{
                    gameOver('Ce n\'était pas ça !');
                }

                if(score > bestScore) {
                    bestScore = score;
                    localStorage.setItem('bestScore', bestScore);
                    updateBestScore();
                }

                for(answerButton of answerButtons) {
                    answerButton.removeAttribute('disabled');
                    answerButton.classList.remove('success');
                    answerButton.classList.remove('error');
                }
            }, 1200);
        })
    }
    nextMovie()
}

// Listen start button
startButton.addEventListener('click', start, {
    once :true,
});

// Pick random element in array
const pickRandomElements = (array, n) => {
    array.sort(() => Math.random() - 0.5);
    return array.slice(0, n);
};

// Init challenge
let timer;
const nextMovie = () => {
    clearInterval(timer);
    let countdown = 15;
    timer = setInterval(() => {
        countdown = countdown - 1;
        timerContainer.innerText = countdown;
        console.log(countdown);
        if(countdown===0) {
            gameOver('Vous avez mis trop de temps...');
            timerContainer.innerText = '15';
        }
    }, 1000);

    timerContainer.innerText = '15';

    displayScore.innerText = score;
    const moviesChosen = pickRandomElements(movies, 4);
    goodAnswer = pickRandomElements(moviesChosen, 1)[0];
    moviesChosen.sort(() => Math.random() - 0.5);
    console.log(moviesChosen);
    console.log(goodAnswer);
    summaryParagraph.innerText = goodAnswer['summary'];

    let counter = 0;
    for(let answerButton of answerButtons){
        answerButton.innerText = moviesChosen[counter].title;
        counter++;
    }
}

// Manage game over
const gameOver = (message) => {
    challengeContainer.classList.add('hide');
    endContainer.classList.remove('hide');
    displayFinalScore.innerText = score;
    restartButton.addEventListener('click', restart);
    failMessage.innerText = message;
    clearInterval(timer);
}

// Manage restart
const restart = () => {
    score = 0;
    endContainer.classList.add('hide');
    challengeContainer.classList.remove('hide');
    nextMovie();
}

// Update best score
const updateBestScore = () => {
    if(bestScore !== 0) {
        for(let containerBest of containersBestScores) {
            containerBest.classList.remove('hide');
        }
        for(let spanBestScore of displayBestScore) {
            spanBestScore.innerText = bestScore;
        }
    }
}

// Récupération local storage si existant
updateBestScore();

// Toggle switch color mode
const colorToggle = document.getElementById('toggle');

colorToggle.addEventListener('change', () => {
    if (colorToggle.checked) {
        document.body.classList.add('darkmode');
    } else {
        document.body.classList.remove('darkmode');
    }
});
